const amadeusService = require('../services/amadeus-service');

class AmadeusController {

    constructor() {
    }

    static test(req, res) {
        amadeusService.test1(result => {
            res.status(200).send(result);
        }).catch(err => {
            res.status(500).send({ error: err })
        })
    }
}

module.exports = AmadeusController;