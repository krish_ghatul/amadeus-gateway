const express = require('express');
const router = express.Router();
const AmadeusController = require('../controller/amadeus-controller');

router.get('/travel', AmadeusController.test);

module.exports = router;
